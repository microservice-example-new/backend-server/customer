'use strict';

const grpc = require('grpc');

const path = require('path');

global._projectDir = __dirname;
global._commonsDir = path.join(__dirname, "../../common-helpers");
global._protobufDir = path.join(__dirname, "../../../application-protos");

require('dotenv').config({path: path.join(global._projectDir, "conf/.env")});
const protoHelper = require(`${global._commonsDir}/helpers/proto-loader.helper`);
const customerCtrl = require('./lib/modules/customer');
const db = require(`${global._commonsDir}/helpers/mongo-database.policy`);

try {
    let server = new grpc.Server();
    const dbConnection = db.init();

    console.log(dbConnection, "-=======");
    const proto = protoHelper.connectGrpc(grpc, 'protos/customer_rpc.proto');

    server.addService(proto.application.customer_rpc.CustomerRpc.service, {
        registerCustomer: customerCtrl.registerCustomer,
        getCustomer: customerCtrl.getAllCustomerInfo
    });

    server.bind(`0.0.0.0:${process.env.PORT}`,
        grpc.ServerCredentials.createInsecure());

    server.start();
    if (server.started) {
        console.log(server, " ============")
        server.call = " adasda";
        console.log(server, " ============")

        console.log(`listening to port 0.0.0}:${process.env.PORT}`);
    }
}catch(err) {
    console.log(err)
}