'use strict';

(()=> {
    const dbase = require(`${global._commonsDir}/helpers/mongo-database.policy`);

    module.exports = async(call, callback) => {
        try {
            await dbase.connectDataBase(call);

            const getAllUser = await call.db.collection('customer').find().toArray();
            return callback(null, {
                    customerList: getAllUser
                
            });
        } catch(err) {
            console.log(err, "err")
            return callback(null, {
                error: true,
                msg: "Error From Create Customer"
            });
        }
    }
})();