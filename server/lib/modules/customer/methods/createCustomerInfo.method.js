'use strict';

(()=> {
    const dbase = require(`${global._commonsDir}/helpers/mongo-database.policy`);

    module.exports = async(call, callback) => {
        try {
            await dbase.connectDataBase(call);
            console.log(call, "===============")

            const { firstName, middleName, lastName, email} = call.request;
            const saveUser = await call.db.collection('customer').insertOne({
                firstName, middleName, lastName, email
            });
            return callback(null, {
                error: false,
                msg: "Customer Saved Successfully"
            });
        } catch(err) {
            console.log(err, "err")
            return callback(null, {
                error: true,
                msg: "Error From Create Customer"
            });
        }
    }
})();