'use strict';

(() => {
    module.exports = {
        getAllCustomerInfo: require('./methods/getAllCustomerInfo.method'),
        registerCustomer: require('./methods/createCustomerInfo.method')
    }
})();